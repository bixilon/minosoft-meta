# Minosoft Meta

This repository is (currently) for pre-flattening data for [Minosoft](https://gitlab.bixilon.de/bixilon/Minosoft).

## Versions

Generally minosoft tries to use the version that is best suited. Because the ids of blocks, ... are really reliable, most things are defined in a fallback folder (`_`). However entities (especially their data) received a major change in 1.9, in a way where it needs to get extracted (its a good thing, however bad for us). Therefore that data is put on hold, and when pixlyzer is ported to a lower version (there is an experimental version for 1.12.2, but forget about it, I just used it for entity data) that data might be used.

## Support

This repository targets version versions from the early beginning (since ids where introduced) until 1.12.2, specifically 17w46a. However nobody bothers about old snapshots or old versions, I will only support `1.7.10`, `1.8.9` and `1.12.2`. Feel free to write custom mappings for any version you need, I will merge them.

## Usage

This repository itself has nothing todo with minosoft, the data can be used for any other project (beware of the license). So updates/changes don't require changes in code, they only need to get compiled and the assets index needs to be updates in minosoft.

## Compiling

This repository just contains beautified mappings. However they are not that usable (in a backwards compatible way), so I wrote a custom "compiler", that creates an assets index, compresses and converts the data (into [mbf](https://gitlab.bixilon.de/bixilon/mbf-specification). The assets index can the be used to list all available data and calculate the best matching version for it.

## Publishing

All compiled data is published into another repository ([minosoft-meta-bin](https://gitlab.bixilon.de/bixilon/minosoft-meta-bin)). Feel free to acquire the compiled data there.

## Handling name changes

All names from versions of 1.13+ are preferred over the old names.

## Source

You might think, wow, this data is insane, I really need that. From where do you have that data? The answer might sound stupid, but most of it is hand written. Some data was extracted with pixlyzer, but not much. So the source is minecraft with (`F3`+`H`) and `/setblock` and the [minecraft wiki](https://minecraft.fandom.com/wiki/Minecraft_Wiki). I am crazy, I know.
